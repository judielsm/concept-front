<?php

    require('vendor/autoload.php');
    require('base/client.php');
    $client = new Base\Client();
    $body = $client->getData();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p><?php echo $body->name; ?></p>
    <p><?php echo $body->description; ?></p>
    <p><?php echo $body->url; ?></p>
    <p><?php echo $body->language; ?></p>
</body>
</html>