<?php
namespace Base;
use \GuzzleHttp;

class Client
{
    private $client;
    private $url;
    public function __construct($url = "https://api.github.com/repos/guzzle/guzzle")
    {
        $this->client = new \GuzzleHttp\Client();
        $this->url = $url;
    }

    public function getData()
    {
        $res = $this->client->get( $this->url );
        return json_decode($res->getBody()->getContents());
    }
}

?>